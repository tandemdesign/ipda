<?php 
// USED AS 'PARENT' PAGE AND OUTPUTS LINKS TO CHILDREN IF THEY EXIST, ELSE JUST OUTPUTS BODY COPY
include 'inc/head.php';
$form = $pages->get('/become-a-member/');
$resources = $page->children();
?>

<?php if($page->url != $form->url){?>
<h2><?=$page->title?></h2>
<?=$page->body?>

<?php if($resources){?>
<div class="tiles">
	<?php foreach($resources as $r){
		$thumb;
		if($r->images){
			$thumb = $r->images->first->url;
		} 
		?>
	<div>
		<a href="<?=$r->url?>"><div class="preview" style="background-image:url('<?=$thumb?>');">
	</div>
		<h3><svg><use xlink:href="#ipdaIcon"></use></svg> <?=$r->title?></h3></a>
	</div>
	<?php }?>
	
</div>

<?php }?>



<?php } else {
	// just body copy
	echo $page->body;
	
}

include 'inc/foot.php'; ?>