//main.js

$(document).ready(function(){
	$('.fancy').fancybox();

	$('#openForm').click(function(){
		$thisForm = $(this).attr('href');
		$($thisForm).toggleClass('hide');
	});

	$('.event').click(function(){
		var label = this.dataset.filename;
		//console.log(label);
		gtag('event', 'click', {
			'send_to': 'UA-29752139-2',
  			'event_category': 'PDF Downloads',
  			'event_label': label
		});

	});
});

