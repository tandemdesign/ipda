<?php 
// RESOURCE GALLERY TEMPLATe
// used for non-videos
include 'inc/head.php';
$loginForm = $pages->get('/member-login/')->url;
$membersOnly = false;
if($page->rootParent->template == 'member-gallery'){
	$membersOnly = true;
}


// check if page is members only
// **template is not used for members only currently**
if($membersOnly == true){
	// if page is members only, check if user is member
	if($user->isLoggedin()){ 
	// if user is member, show content?>

	<h2><?=$page->title?></h2>
	<?=$page->body?>

	<?php foreach($page->children() as $resource){
		$thumb = $resource->images->first(); ?>
	<div class="single">
		<?php if($thumb){?><a class="thumbnail" href="<?=$resource->url?>">
			<img src="<?=$thumb->size(210,250)->url?>" alt="<?=$resource->title?>" alt="<?=$resource->title?>" />
		</a><?php } ?>
		<div class="description">
			<p><strong><a href="<?=$resource->url?>"><?=$resource->title?></strong></a></p>
			<?=$resource->body?>
			<p><a class="more-info" href="<?=$resource->url?>" title="Learn More"><svg><use xlink:href="#ipdaIcon"></use></svg>Learn More</a></p>
			
		</div><!--description-->
	</div><!--resource-->
	<?php } ?>

	<p><a class="main-link" href="<?=$page->parent->url?>"><svg><use xlink:href="#ipdaIcon"></use></svg>Return to <?=$page->parent->title?></a></p>
	<?php 
	} else {
	// if user is not member, redirect to login form page 
		$session->redirect($loginForm.'?id='.$page->id);
	}
} else { // if page is not members only, show content to everyone  ?>
	<h2><?=$page->title?></h2>
	<?=$page->body?>

	<?php foreach($page->children() as $resource){
	$thumb = $resource->images->first(); ?>
	<div class="single">
		<?php if($thumb){?><a class="thumbnail" href="<?=$resource->url?>">
			<img src="<?=$thumb->size(210,250)->url?>" alt="<?=$resource->title?>" alt="<?=$resource->title?>" />
		</a><?php } ?>
		<div class="description">
			<p><strong><a href="<?=$resource->url?>"><?=$resource->title?></strong></a></p>
			<?=$resource->body?>
			<p><a class="more-info" href="<?=$resource->url?>" title="Download PDF"><svg><use xlink:href="#ipdaIcon"></use></svg>Learn more</a></p>
			
		</div><!--description-->
	</div><!--resource-->
	<?php } ?>

	<p><a class="main-link" href="<?=$page->parent->url?>"><svg><use xlink:href="#ipdaIcon"></use></svg>Return to <?=$page->parent->title?></a></p>

<?php }
include 'inc/foot.php';?>