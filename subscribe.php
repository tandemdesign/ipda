<?php
include 'inc/head.php';
//subscribe for more updates form, goes to info@ipda.ca
echo $page->body;
$form = $modules->get('FormTemplateProcessor'); 
$form->template = $templates->get('contact-form'); // USE 'contact-form' TEMPLATE
$form->requiredFields = array('first_name','last_name','company','job_title','email','phone');
$form->email = 'info@ipda.ca'; // optional, sends form as email
$form->parent = $page; // optional, saves form as page
echo '<div class="form-header"><h2><svg><use xlink:href="#ipdaLogo"?></use></svg> '.$page->title.'</h2></div>';
echo $form->render(); // draw form or process submitted form
include 'inc/foot.php'; 



?>

