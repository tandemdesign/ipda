<?php 
// REGISTER NEW MEMBER FORM CURRENTLY BEING TESTED BY CLIENT

// get list of allowed companies from IPDA Members page
$companies = $pages->get('/about-us/ipda-members/')->member->sort('title'); // sort by title, case insensitive
$message = "";

//output when submitted
if($input->post->submit){

	//get input values
	$email = $input->post->email;
	$company = $input->post->company;
	$username = $input->post->user;
	$current_users = wire('users');
	
	//all fields are required
	if($email && $username && $company != " "){
		//get domain of provided email
		list($first,$domain) = explode("@", $email); 
		//find IPDA members that match company title and accepted domains (emails are in $company->headline field)
		$matchedCompanies = $companies->find("headline~=".$domain.", title=".$company); 
		//make sure username already doesn't exist
		if($current_users->get("$username")->id){
			$message = "<p class='error'>That username already exists.</p>";
			//if provided email domain matches allowed domain under the chosen company
		} else if(count($matchedCompanies)) {
			//print_r("success!");
			//create new user, send welcome email to new user
			$modules->get("EmailNewUser");
			$newMember = new User();
			$newMember->name = $username;
			$newMember->email = $email;
			$newMember->company = $company;
			$newMember->addRole("member");
			$newMember->sendEmail = true;
			$newMember->save();
			$message = "<p class='success'>Success! Check your email for instructions to log in.</p>";
		} else {
			//print_r("fail!");
			$message = "<p class='error'>Please use your company email.</p>";
		}

	} else {
		$message = "<p class='error'>All fields are required.</p>";
	}
	
	
	
}
include 'inc/head.php';

// user already logged in
if($user->isLoggedin()){?>
	<p>You are logged in as <strong><?=$user->name?></strong>.</p>
	<p>Not you? <a href="<?=$pages->get('/logout/')->url?>">Click here to log out</a>.</p>
<?php } else { echo $page->body; } ?>
	
	<form action='./' method='post'>
		<div class="login-form">
			<h2><svg><use xlink:href="#ipdaLogo"></use></svg> User Registration</h2>		
			<?=$message?>
			<p><label for="email">Email</label> <input type="text" name="email" id="email" required /></p>
			
			<p><label for="user">Username</label> <input type="text" name="user" id="user" required /></p>
			<p><label for="company">Company</label>
			<select name="company" id="company" required>
				<option value=" "> </option>
				<?php foreach($companies as $company){  
				if($company->headline){ ?>
				<option value="<?=$company->title?>"><?=$company->title?></option>
				<?php }
			}?>
			</select>
			</p>
			<input type="submit" name="submit" value="Sign up" />
			<?php if(!$user->isLoggedin()){ //show link to log in page if not logged in ?>
			
			<p>Already a member? <a title="Member Login" href="<?=$pages->get('/member-login/')->url?>">Click here</a>. </p>
			<div class="clearfix"></div>
			<?php }?>
		</div>
	</form>


<?php include 'inc/foot.php'; ?>