<?php
// RESOURCE SINGLE TEMPLATE
// NON-VIDEOS
include 'inc/head.php'; 
$loginForm = $pages->get('/member-login/')->url;
$thumb; 
?>



<div class="single">
		<a class="thumbnail" href="<?=$page->file->url?>">
			<?php if(count($page->images) > 0){
			$thumb = $page->images->first()->size(210,250)->url;
			} else {
			$thumb = $config->urls->templates . 'dest/img/file-thumb.jpg';
			}?>
			<img src="<?=$thumb?>" alt="<?=$page->title?>" />

		</a>
		<div class="description">
			<p><strong><?=$page->title?></strong></p>
			<?=$page->body?>
			<?php 
			//for each file, add link to it
			foreach($page->file as $pdf){
			$linkTitle = ($pdf->description == '') ? 'Download full report' : $pdf->description;
			?>
			<p><a class="more-info event" data-filename="<?=$page->title?>" href="<?=$pdf->url?>" title="Download PDF"><svg><use xlink:href="#ipdaIcon"></use></svg><?=$linkTitle?> (<?=$pdf->filesizeStr?>)</a></p>
			<?php } 
			if($page->link){?>
			<p><a class="more-info event" target="_blank" data-filename="<?=$page->title?>" href="<?=$page->link?>" title="Download PDF"><svg><use xlink:href="#ipdaIcon"></use></svg><?=$page->last_name?></a></p>
			<?php }
			?>
		</div><!--description-->
	</div><!--resource-->

<p><a class="main-link" href="<?=$page->parent->url?>"><svg><use xlink:href="#ipdaIcon"></use></svg>Return to <?=$page->parent->title?></a></p>

<?php


include 'inc/foot.php';?>