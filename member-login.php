<?php 
// MEMBER LOGIN TEMPLATE

$requestID = $input->get['id']; //if user was trying to see page, get id of page user wants to access
$requestSrc = $input->get['src']; //if user trying to see forums

//check for login before outputting markup
if($input->post->user && $input->post->pass){


	$userInput = $sanitizer->username($input->post->user); //get username from form 
	$pass = $input->post->pass; //get password from form 

	//if login successful
	if($session->login($userInput,$pass)){
		

		//check if came from previous page
		if($requestID){
			//redirect to previous page
			
			$t = $pages->get($requestID)->httpUrl;
			$session->redirect($t);

		} else if($requestSrc == 'forums') {
			//if trying to get into forums, redirect to forums
			$session->id = $user->id;
			$session->name = $user->name;
			$session->email = $user->email;

			header('location:talk/sso');

		} else {
			// just redirect to homepage

			$session->redirect($pages->get('/')->url);
		}
		

		
	}
}
include 'inc/head.php';
// user already logged in
if($user->isLoggedin()){?>
	
	<p>You are logged in as <strong><?=$user->name?></strong>.</p>
	<p>Not you? <a href="<?=$pages->get('/logout/')->url?>">Click here to log out</a>.</p>
<?php } else { echo $page->body; } ?>

	<form action='./?id=<?=$requestID?>' method='post'>
		<div class="login-form">
			<h2><svg><use xlink:href="#ipdaLogo"></use></svg> Member Login</h2>
			<?php if($input->post->user) echo '<p class="error">Login failed. Please try again.</p>'; ?>
			<p><label for="user">Username</label> <input type="text" name="user" id="user"/></p>
			<p><label for="pass">Password</label><input type="password" name="pass" id="pass"/></p>
			<input type="submit" name="submit" value="Login" />
			<?php if(!$user->isLoggedin()){ //show forgot password if not logged in ?>
			
			<p>Forgot password? <a title="Click here if you forgot your password" href="<?=$pages->get('/forgot-password/')->url?>">Click here</a>. </p>
			<div class="clearfix"></div>
			<?php }?>
		</div>
	</form>


<?php include 'inc/foot.php'; ?>