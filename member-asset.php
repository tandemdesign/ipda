<?php 
// RESOURCE GALLERY FOR MEMBERS ONLY SECTION
// UPLOADED FILES APPEAR AS LIST UNDER BODY COPY
// FILES ARE SECURE AND LOCATED OUTSIDE OF ROOT DIRECTORY 
include 'inc/head.php';
$loginForm = $pages->get('/member-login/')->url;
//check if user is logged in and show page else redirect to log in form
if($user->isLoggedin()){ 
?>
	<h2><?=$page->title?></h2>

	<div class="single">
		<div class="description">
				<?=$page->body?>
				<?php if(count($page->member_file)){
				foreach($page->member_file as $file){?>
					<h3><?=$file->title?></h3>
						<p><?=$file->body?></p>
						<p><a class="more-info" href="?filename=<?=$file->secure_file->name?>" title="Download PDF"><svg><use xlink:href="#ipdaIcon"></use></svg>Download Document (<?=$file->secure_file->filesizeStr?>)</a></p>
						<?php 
						if($input->get['filename'] == $file->secure_file->name && $user->isLoggedin()){
							$file->secure_file->download();
						}
					}
				}?>
		</div><!--description-->
	</div>
<?php 
} else {
	$session->redirect($loginForm.'?id='.$page->id);
}



include 'inc/foot.php';?>
