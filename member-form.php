<?php
// MEMBER FORM - SIGN UP FOR MORE INFORMATION ON BECOMING A MEMBER
include 'inc/head.php';
echo '<h2>Get Information on Becoming an IPDA Member</h2>';
echo $page->body;
$form = $modules->get('FormTemplateProcessor'); 
$form->template = $templates->get('form'); //USE 'form' TEMPLATE
$form->requiredFields = array('first_name','last_name','company','job_title','email','phone');
$form->email = 'info@ipda.ca'; // optional, sends form as email
$form->parent = $page; // optional, saves form as page
echo '<div class="form-header"><h2><svg><use xlink:href="#ipdaLogo"?></use></svg> Information Sign Up</h2></div>';
echo $form->render(); // draw form or process submitted form
include 'inc/foot.php'; 



?>

