# IPDA Website - Templates directory

Built in Processwire. Members-only content is only viewable if user has role of superuser, client or member. 

Uses [Vanilla forms](https://vanillaforums.com/en/software/), jquery, fancybox 

**Testing URL:** /sitetesting/ipda/

**To Log In:** /processwire

**Live URL:** ipda.ca

## Compile steps 

Sass: 

`$ gulp styles`

JS:

`$ gulp scripts`

Images:

`$ gulp images`

Watch: 

`$ gulp watch`

**Source Directory:** /src

**Dest Directory:** /dest


## Template guide: 

- basic-page: "Parent" page - shows thumbnails of child pages (if they exist, otherwise it's just body copy)
- contact: "Members listing" pages - ex: IPDA Members, Board Members. Named contact because of previous design decisions that got changed 
- contact-form: "subscribe to updates" form, sends to info@ipda.ca
- event-single: Single event page, client is able to add them
- events: Event parent page, returns all events
- form: "Member Info" form 
- forum: [VANILLA FORUMS](https://vanillaforums.com/en/software/)
- home: Homepage
- logout: logs user out
- media: Currently not being used 
- media-gallery: parent page of video gallery
- media-single: single video
- member-asset: currently not being used. was originally supposed to have "members-only" galleries. don't exist anymore.
- member-gallery: all children are members-only viewable
- member-login: login page 
- member-profile: view username/email. Change password, etc
- member-register: people with whose company is an IPDA member can sign up. Matches provided email with company email they pick.
- password-reset: reset password page [Module info](https://github.com/plauclair/PasswordReset)
- password-reset-request: saves password reset requests 
- resource: Resource gallery parent/category
- resource-single: add PDF/Case study


## How to add Members to IPDA Member Resources

1. Login to the Processwire admin panel: ipda.ca/processwire
2. At top of page, click Setup > Admin Actions > Create Users Batcher
3. Select “member” from Roles dropdown menu
4. Input the new users’ info (1 line per user) in the text area at the bottom of page. Each line must contain a username you wish to assign them and their email address. (ex: username, user@example.com). Each field is separated by a comma and each user is separated by one line.

Click Execute Action and the users will be created and an email will be automatically sent out to all newly created users with instructions to log in. 

### Spreadsheets

Spreadsheets of users can also be added. 

Open the spreadsheet in Excel (or whatever program you use) and make sure the columns are in the same order as stated above (username, email). 

Save the file as a .csv (Save As > Format: Comma Separated Values (.csv)). 

Open the newly created .csv in a text editor (Word, Notepad, etc). Here you can copy and paste this into the New Users textarea in Processwire. 


## Registration guide:

Company domain emails are added in Processwire in the IPDA Members page. In each IPDA member there is an input for acceptable domains, separated by commas.