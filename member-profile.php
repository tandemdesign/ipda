<?php 
// MEMBER PROFILE PAGE
include 'inc/head.php';

//check if user logged in
if($user->isLoggedin()) {

if($input->post->update) {

    //instantiate variables taking in the form data
    $email = $sanitizer->email($input->post->changeEmail);

    //Update user details
      $user->of(false);
      $user->email = $email;
      $user->save();
      $user->of(true);
}?>
<div class="form-header">
	<h2><svg><use xlink:href="#ipdaLogo"></use></svg> Member Profile</h2>
</div>
<div class="profile-wrapper">
	
	<p><strong>Username:</strong> <?=$user->name?></p>
	<p><strong>Email:</strong> <?=$user->email?></p>
	
	<div id="resetEmail" class="hide">
		<form action='./' method='post' autocomplete='off'>
			<div class="login-form">

				<p><input type="email" name="changeEmail" id="changeEmail" value="<?=$user->email?>" /><label for="user">New Email</label> </p>
				<input type="submit" name="update" value="Update" />
				
			</div>
		</form>

	</div>
	<p><strong>Account Actions:</strong><br />
	<a id="openForm" href="#resetEmail">Update Your Email</a> / 
	<a href="<?=$pages->get('/forgot-password/')->url?>">Reset Your Password</a>
	</p>
	<a href="<?=$pages->get('/member-resources/member-forum/')->url?>" class="green-btn">Visit Member Forum »</a>

</div><!--profile-wrapper-->
<?php } else {
	$session->redirect($pages->get('/member-login/')->url);
}?>


<?php include 'inc/foot.php'; ?>