<?php 
	//check if logged in, if logged in, REDIRECT to VANILLA FORUMS (ipda.ca/talk)
	if($user->isLoggedin()){
		$role;
		// sends proper role name to Vanilla Forums
		if($user->hasRole('client') || $user->isSuperuser()){
			$role = 'administrator';
		} else {
			$role = 'member';
		}
		$session->id = $user->id;
		$session->name = $user->name;
		$session->email = $user->email;
		$session->role = $role;
		//echo "<script type='text/javascript'>alert('$session->email');</script>";
		header( "location:../../talk" );
	} else {
		//if not logged in, redirect to login page
		$session->redirect($pages->get('/member-login/')->url.'?id='.$page->id);
	}

?>