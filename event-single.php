<?php 
// SINGLE EVENT TEMPLATE, CLIENT IS ABLE TO ACCESS
include 'inc/head.php';

$page->body

?>

<div class="single">
	<a class="thumbnail" href="<?=$page->link?>"><img src="<?=$page->images->first->url?>" alt="<?=$page->title?>" /></a>
	<div class="description">
		<p><strong><?=$page->title?></strong><br />
		<?=$page->headline?><br />
		<?php 
		if (count($page->additional_dates)){?>
		<ul>
			<li><?=$page->date?> <?=$page->time?></li>
		<?php foreach($page->additional_dates as $addDate){?>
			<li><?=$addDate->date?> <?=$addDate->time?></li>
		<?php }?>
		</ul>

		<?php } else { ?>
		<?=$page->date?> <?=$page->time?><br />
		<?php }?>
		

		<?php if($page->link){ echo "<a class='more-info' href=$page->link title='Buy Tickets'><svg><use xlink:href='#ipdaIcon'></use></svg>Buy Tickets</a></p>"; }?>
		<?=$page->body?>
	</div><!--description-->
	
</div><!--event wrapper-->

<p><a class="main-link" href="http://www.ipda.eventbrite.com/"><svg><use xlink:href="#ipdaIcon"></use></svg>Click to view full Events Calendar</a></p>

<?php 
include 'inc/foot.php'; 
?>