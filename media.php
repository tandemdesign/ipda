<?php
// MEDIA MAIN TEMPLATE
include 'inc/head.php';
//$media = $pages->find('template=media-single, members_only=0');

$galleries = $page->children;
?>
<h2><?=$page->title?></h2>
<?php
foreach($galleries as $gallery){
?>
	<h3><svg><use xlink:href="#ipdaIcon"></use></svg> <?=$gallery->title?></h3>
	<div class="gallery-container">
	<?php foreach($gallery as $media){?>
	<div class="single">
		<a class="thumbnail" href="<?=$media->url?>"><img src="<?=$media->thumbnail->eq(0)->getThumb('thumbnail');?>" alt="<?=$media->title?>" /></a>
		<div class="description">
			<p><strong><a href="<?=$media->url?>"><?=$media->title?></strong></a></p>
			<?=$media->body?>
		</div><!--description-->
	</div><!-- single-->

	<?php } ?>
	</div><!--gallery-container-->
<?php } ?>

<!--<p><a class="main-link" href="<?=$memberGallery->url?>" title="View our member resource gallery"><svg><use xlink:href="#ipdaIcon"></use></svg>View our member resource gallery</a></p>-->


<?php include 'inc/foot.php';?>