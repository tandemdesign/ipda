<?php 
// MEDIA GALLERY TEMPLATE
//used for videos only
include 'inc/head.php';
$first = $page->children->first();
$media = $page->children("start=1, limit=50");
$loginForm = $pages->get('/member-login/')->url;
$membersOnly = false;
if($page->rootParent->template == 'member-gallery'){
	$membersOnly = true;
}


// check if page is members only
if($membersOnly == true){
	// if page is members only, check if user is member
	if($user->isLoggedin()){ 
	// if user is member, show content?>

	<h2><?=$page->title?></h2>
	<?=$page->body?>
	<div class="first-single">
		<?php if($first->video){ 
		$vidLink = strip_tags($first->video); ?>
			
		<iframe src="<?=$vidLink?>" frameborder="0" allowfullscreen></iframe>
		<?php } ?>
		<div class="description">
			<p><strong><?=$first->title?></strong><br />
				<?=$first->body?></p>
		</div>
	</div><!--first-single-->
	
	
	<?php foreach($media as $m){ 
	$img; ?>
		<div class="single-box">
			<a class="thumbnail" href="<?=$m->url?>">
				<?php if(count($m->images) > 0){
				$img = $m->images->first()->size(200, 115)->url; 
				} else {
				$img = $config->urls->templates . 'dest/img/gallery-thumb.jpg';
				}?>
				<img src="<?=$img?>" alt="<?=$m->title?>" />
				<p><strong><?=$m->title?></strong></p>
			</a>
		</div><!-- single-->	
		
	<?php } 
	} else {
		// if user is not member, redirect to login form page 
		$session->redirect($loginForm.'?id='.$page->id);
	} 
} else { // if page is not members only, show content to everyone  ?>

<h2><?=$page->title?></h2>
<?=$page->body?>
<div class="first-single">
	<?php if($first->video){ 
		echo $first->video; 
	} ?>
	<div class="description">
		<p><strong><?=$first->title?></strong><br />
			<?=$first->body?></p>
	</div>
</div><!--first-single-->


<?php foreach($media as $m){
	$img = $m->images->first(); ?>
	<div class="single-box">
		<a class="thumbnail" href="<?=$m->url?>"><img src="<?=$img->size(200, 115)->url?>" alt="<?=$m->title?>" />
		<p><strong><?=$m->title?></strong></p>
		</a>
	</div><!-- single-->	
	
<?php }
}?>





<?php include 'inc/foot.php';?>