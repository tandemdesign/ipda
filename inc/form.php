<!--<form name="membership-form" method="post" action="/">
	<div class="login-form ">
	<h2><svg><use xlink:href="#ipdaLogo"?></use></svg> Become a member today</h2>
	
	<p><input type="text" name="firstname" id="firstname"/><label for="firstname">First Name</label> </p>
	<p><input type="text" name="lastname" id="lastname"/><label for="lastname">Last Name</label> </p>
	<p><input type="text" name="company" id="company"/><label for="company">Company</label> </p>
	<p><input type="text" name="phone" id="phone"/><label for="phone">Phone Number</label> </p>
	<p><input type="text" name="email" id="email"/><label for="email">Email</label> </p>
	
	<input type="submit" name="submit" value="Submit" />
	<div class="clearfix"></div>
</div>
</form>
-->
<?php

$form = $modules->get('FormTemplateProcessor'); 
$form->template = $templates->get('my_contact_form_template'); // required
$form->requiredFields = array('fullname', 'email');
$form->email = 'your@email.com'; // optional, sends form as email
$form->parent = $page; // optional, saves form as page
echo $form->render(); // draw form or process submitted form

?>