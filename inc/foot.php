</div><!--container-->
<div class="footer">
	<div class="container">
		<svg class="logo" alt="IPDA"><use xlink:href="#ipdaLogoText"></use></svg>
		<p><?=$home->headline?></p>
		<div class="contact">
			<p>
        <a href="<?=$pages->get('/subscribe/')->url?>" title="<?=$pages->get('/subscribe/')->title?>"><?=$pages->get('/subscribe/')->title?></a> 
        <a href="mailto:info@ipda.ca" title="Contact us">Contact Us</a> 
        <a href="<?=$footBtnURL?>"><?=$footBtnName?></a>
			</p>
		</div>
	</div><!--container-->
</div><!--footer-->
<!-- ANALYTICS -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?=$config->urls->templates?>dest/scripts/main.min.js"></script>
</body>
</html>