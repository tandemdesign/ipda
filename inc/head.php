<?php 
$home = $pages->get('/');
//$memberGallery = $pages->get('/member-resources/');
$options = array(
        'parent_class' => 'parent',
        'current_class' => 'current',
        'has_children_class' => 'has_children',
        'levels' => true,
        'levels_prefix' => 'level-',
        'max_levels' => 2,
        'firstlast' => false,
        'collapsed' => false,
        'show_root' => false,
        'selector' => 'parent!=1051, id!=1073, parent!=1079, id!=1236, id!=1492, id!=1079, parent!=1500, id!=1505',
        'selector_field' => 'nav_selector',
        'outer_tpl' => '<ul>||</ul>',
        'inner_tpl' => '<ul>||</ul>',
        'list_tpl' => '<li%s>||</li>',
        'list_field_class' => '',
        'item_tpl' => '<a href="{url}">{title}</a>',
        'item_current_tpl' => '<a href="{url}">{title}</a>',
        'xtemplates' => '',
        'xitem_tpl' => '<a href="{url}">{title}</a>',
        'xitem_current_tpl' => '<span>{title}</span>',
        'date_format' => 'Y/m/d',
        'code_formatting' => false,
        'debug' => false
    );
$treeMenu = $modules->get("MarkupSimpleNavigation");
$headBtnName;
$headBtnURL;
$footBtnName;
$footBtnURL;
//update login btn and sign out btn text
if($user->isLoggedin()){ 
    $headBtnName = '<svg><use xlink:href="#userIcon"></use></svg> '.$user->name;
    $headBtnURL = $pages->get('/member-profile/')->url;
    $footBtnName = 'Sign Out';
    $footBtnURL = $home->url."talk/entry/signout";
} else {
    $headBtnName ='Member Login';
    $headBtnURL = $pages->get('/member-login/')->url; 
    $footBtnName = 'Member Login';
   $footBtnURL = $pages->get('/member-login/')->url;
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title><?=$page->title?> | The Integrated Project Delivery Alliance (IPDA)</title>
		<meta name="description" content="<?php if($page->meta_description){ echo $page->meta_description;} else { echo 'The Integrated Project Delivery Alliance (IPDA) is a group of organizations that seek to advance Integrated Project Delivery (IPD) as a delivery method in Canada. Members support one another as they seek-out, implement and promote best practices that enable us to build better together.';} ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="author" href="<?=$config->urls->templates?>humans.txt">
		<link rel="stylesheet" type="text/css" href="<?=$config->urls->templates?>dest/styles/main.min.css" />
        <link rel="apple-touch-icon" sizes="57x57" href="<?=$config->urls->templates?>apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?=$config->urls->templates?>apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?=$config->urls->templates?>apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?=$config->urls->templates?>apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?=$config->urls->templates?>apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?=$config->urls->templates?>apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?=$config->urls->templates?>apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?=$config->urls->templates?>apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?=$config->urls->templates?>apple-touch-icon-180x180.png">
        <link rel="icon" type="image/png" href="<?=$config->urls->templates?>favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?=$config->urls->templates?>android-chrome-192x192.png" sizes="192x192">
        <link rel="icon" type="image/png" href="<?=$config->urls->templates?>favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="<?=$config->urls->templates?>favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="<?=$config->urls->templates?>manifest.json">
        <link rel="mask-icon" href="<?=$config->urls->templates?>safari-pinned-tab.svg" color="#009683">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="msapplication-TileImage" content="<?=$config->urls->templates?>mstile-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <meta property="og:image" content="<?=$config->urls->templates?>dest/img/ipda-fb-01.jpg">
        <meta property="og:image" content="<?=$config->urls->templates?>dest/img/ipda-fb-02.jpg">
        <meta property="og:image" content="<?=$config->urls->templates?>dest/img/ipda-fb-03.jpg">
        <meta name="twitter:image" content="<?=$config->urls->templates?>dest/img/ipda-fb-02.jpg">
	</head>
<body class="<?=$page->template?>">
<?php 
include_once 'svg/all.svg';
include 'inc/tracking.php';
?>	
<div class="header">
	<div class="content">
		<a href="<?=$home->url?>" title="IPDA"><svg class="logo" title="IPDA Logo"><use xlink:href="#ipdaLogo"></use></svg></a>
		<h1><span class="black">The</span> <span class="green">Integrated Project Delivery</span> Alliance</h1>
        
	</div><!-- content-->
</div><!--header-->
<div class="hero">

   
	<div class="social">
		<ul>
            <li class="user"><a href="<?=$headBtnURL?>"><?=$headBtnName?></a>
			<li><a target="_blank" href="https://twitter.com/IPDA_Team"><svg><use xlink:href="#twitterIcon"></use></svg></a></li>
			<li><a target="_blank" href="https://www.instagram.com/ipda_team/"><svg><use xlink:href="#instagramIcon"></use></svg></a></li>
            <li><a target="_blank" href="https://www.linkedin.com/company/integrated-project-delivery-alliance-ipda-?trk=company_logo"><svg><use xlink:href="#linkedinIcon"></use></svg></a></li>
            <li><a target="_blank" href="https://vimeo.com/ipda"><svg><use xlink:href="#vimeoIcon"></use></svg></a></li>
		</ul>
	</div><!--social media-->
	<div class="member-btn">
		<h2>Get Information on Becoming an IPDA Member.</h2>
		<a href="<?=$pages->get('/become-a-member/')->url?>" title="Become a Member of IPDA">Click Here</a>
	</div><!--member btn-->
</div><!--hero-->
<input class="mobile-show" type="checkbox" id="mobile" name="mobile">
<label for="mobile">Menu 〉</label>
<nav>
	<?php echo $treeMenu->render($options); ?>
</nav>

<div class="main container">