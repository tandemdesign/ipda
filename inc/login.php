<?php

// Handle logouts
if($input->get->logout == 1) {
	$session->logout();
	$session->redirect($page->path);
}

// If they aren't logged in, then show the login form
if(!$user->isLoggedin()){

// check for login before outputting markup
if($input->post->user && $input->post->pass) {

	$user = $sanitizer->username($input->post->user);
	$pass = $input->post->pass;

	if($session->login($user, $pass)) {
    // login successful
    	$session->set("error", ""); // note: moved this above the redirect
    	$session->redirect($pages->get('/member-resources/')->url);
	}else {
    	$session->set("error", "Login Failed. Please try again, or use the forgot password link below.");
	}

}

?>
<!doctype html>
<html lang="en">
<head>
	<title>IPDA | Please Log In</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>dest/styles/main.min.css" />
	
</head>

<body class="member-login">

<form action='./' method='post'>
<div class="login-form">
	<h2 style="margin-left:0;"><svg><use xlink:href="#ipdaLogo"?></use></svg> Member Resource Gallery</h2>
	<? if($input->post->user && $input->post->pass) {
	echo "<p class='error'>" . $session->login_error . "</p>";
	}?>
	
	<?php if($input->post->user) echo '<h2>Login failed. Please try again.</h2>'; ?>
	<p><input type="text" name="user" id="user"/><label for="user">Username</label> </p>
	<p><input type="password" name="pass" id="pass"/><label for="pass">Password</label></p>
	<input type="submit" name="submit" value="Login" />
	<p>Forgot password? <a title="Forgot password" href="#">Click here</a>. </p>
	<div class="clearfix"></div>
</div>
</form>
</body>
</html>

<?
die(); // don't go any further if not logged in
} // end !logged in

?>