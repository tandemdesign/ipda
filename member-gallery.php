<?php 
// ALL MEMBERS ONLY STUFF ARE CHILDREN OF THIS PAGE. DISPLAYS MEMBERS ONLY RESOURCES.
// Only users with the role of superuser, client or member can view this
include 'inc/head.php';
$loginForm = $pages->get('/member-login/')->url;

// check if user is member
if($user->isLoggedin()){ 
// if user is member, show content?>
<h2><?=$page->title?></h2>

<?=$page->body?>

	<?php 
	if(count($page->children) > 0){ //if files exist in library
	foreach($page->children as $c){
	$img;
	$btnText;?>
		
	<div class="single">
		<a class="thumbnail" href="<?=$c->url?>">
		<?php if(count($c->images) > 0){
			$img = $c->images->first()->size(350,200)->url;
		} else {
			$img = $config->urls->templates . 'dest/img/gallery-thumb.jpg';
		}?>
			<img src="<?=$img?>" alt="<?=$c->title?>"  />
		
		</a>
		<div class="description">
			<p><strong><a href="<?=$c->url?>"><?=$c->title?></strong></a></p>
			<?=$c->body?>
			<?php $c->link ? $btnText = $c->link : $btnText = 'Learn More'; ?> 
			<p><a class="more-info" href="<?=$c->url?>" title="Open <?=$c->title?>"><svg><use xlink:href="#ipdaIcon"></use></svg><?=$btnText?></a></p>
		
		</div><!--description-->
		
		
	</div><!--single-->
	
	
<?php } //end foreach
	} else { // if library is empty
		echo '<p>Check back soon for exclusive members-only content!</p>';
	}
	
?>

<?php } else {
	// if user is not member, redirect to login form page 
		$session->redirect($loginForm.'?id='.$page->id);
} ?>



<?php include 'inc/foot.php';?>