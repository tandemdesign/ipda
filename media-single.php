<?php 
// MEDIA SINGLE TEMPLATE (videos)
include 'inc/head.php';
$media = $page->parent;
$loginForm = $pages->get('/member-login/')->url;
$membersOnly = false;
if($page->rootParent->template == 'member-gallery'){
	$membersOnly = true;
}
// check if page is members only
if($membersOnly == true){
	// if page is members only, check if user is member
	if($user->isLoggedin()){ 
	// if user is member, show content?>
		<h2><?=$media->title?></h2>
		<div class="first-single">
			<?php if($page->video){ 
			$vidLink = strip_tags($page->video); ?>
			
			<iframe src="<?=$vidLink?>" frameborder="0" allowfullscreen></iframe>
			<?php } ?>
			<div class="description">
				<p><strong><?=$page->title?></strong><br />
					<?=$page->body?></p>
		
			</div>
		</div><!--first-single-->
		
		<?php
		
		foreach($media->children() as $m){ 
		if($m->id != $page->id){
		$img = $m->images->first(); ?>
		<div class="single-box">
			<a class="thumbnail" href="<?=$m->url?>"><img src="<?=$img->size(200, 115)->url?>" alt="<?=$m->title?>" />
				<p><strong><?=$m->title?></strong></p>
			</a>
		</div><!-- single-->	
			
		<?php }
			
		} 
	} else {
		// if user is not member, redirect to login form page 
		$session->redirect($loginForm.'?id='.$page->id);
	}
} else { // if page is not members only, show content to everyone  ?>

<h2><?=$page->parent->title?></h2>
<div class="first-single">
	<?php if($page->video){ 
		echo $page->video; 
	}  ?>sadsd
	<div class="description">
		<p><strong><?=$page->title?></strong><br />
			<?=$page->body?></p>

	</div>
</div><!--first-single-->

	<?php

	foreach($media->children() as $m){ 
	if($m->id != $page->id){
		$img = $m->images->first(); ?>
	<div class="single-box">
		<a class="thumbnail" href="<?=$m->url?>"><img src="<?=$img->size(200, 115)->url?>" alt="<?=$m->title?>" />
			<p><strong><?=$m->title?></strong></p>
		</a>
	</div><!-- single-->	
	
	<?php }
	}

} 

include 'inc/foot.php';?>