<?php 
// ALL EVENTS. LISTS CHILDREN/SINGLE EVENTS
include 'inc/head.php';
echo '<h2>Events</h2>';
echo $page->body;

if (count($page->children)){

	echo '<h3>Upcoming Events</h3>';

	foreach($page->children as $event){
	?>
	<div class="single">
		<a class="thumbnail" href="<?=$event->link?>">
			<?php if($event->images->first){?>
			<img src="<?=$event->images->first->url?>" alt="<?=$event->title?>" />
			<?php } else { ?>
			<img src="<?=$config->urls->templates?>dest/img/event-thumb.jpg" alt="<?=$event->title?>" />
			<?php }?>
		</a>
		<div class="description">
			<p><strong><?=$event->title?></strong><br />
			<?php if($event->headline){ echo $event->headline.'<br />'; }?>
			<?php 
			if (count($event->additional_dates)){?>
			<ul>
				<li><?=$event->date?> <?=$event->time?></li>
			<?php foreach($event->additional_dates as $addDate){?>
				<li><?=$addDate->date?> <?=$addDate->time?></li>
			<?php }?>
			</ul>
	
			<?php } else { ?>
			<?=$event->date?> <?=$event->time?><br />
			<?php }
			if($event->link){ echo "<a class='more-info' href=$event->link title='Buy Tickets'><svg><use xlink:href='#ipdaIcon'></use></svg>Buy Tickets</a></p>"; }?>
		
			<?=$event->body?>
		</div><!--description-->
		
		
	</div><!--event wrapper-->
	<?php } //LINKS TO IPDA'S EVENTBRITE PAGE ?>
	<p><a class="main-link" href="<?=$page->link?>"><svg><use xlink:href="#ipdaIcon"></use></svg>Click to view full Events Calendar</a></p>
	<?php 
	} else { 
	// IF NO EVENTS
	echo '<p><strong>No Upcoming Events, check back soon!</strong></p>';

}?>


<?php include 'inc/foot.php'; ?>