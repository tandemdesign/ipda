<?php 
// PAGE IS USED FOR BOARD MEMBERS PAGE AND IPDA MEMBERS PAGE (used to be contact page)
include 'inc/head.php';
$profiles = $page->profile;
$members = $page->member->sort('title');
?>
<h2><?=$page->title?></h2>
<?=$page->body?>

<?php 
	//BOARD MEMBERS LIST
	if($profiles){
	foreach($profiles as $profile){
	?>
	<div class="single profile">
		<?php
		if($profile->images->first){?>
		<img class="thumbnail" src="<?=$profile->images->first->size(325,450)->url?>" alt="<?=$profile->first_name?> <?=$profile->last_name?> IPDA" /></a>
		<?php } else { ?>
		<svg><use xlink:href="#ipdaIcon"></use></svg>
		<?php }?> 
		<div class="description">
			<p><strong><?=$profile->first_name?> <?=$profile->last_name?></strong><br />
			<?=$profile->title?><br />
			<?=$profile->headline?><br />
			<?php if($profile->phone){?><a href="tel:<?=$profile->phone?>"><?=$profile->phone?></a><br /><?php }?>
			<a class="email" href="mailto:<?=$profile->email?>"><?=$profile->email?></a></p>
		</div><!--description-->
	</div><!--profile-->
<?php } 
} if($members){ 
	//IPDA MEMBERS LIST
	foreach($members as $member){ ?>
	<div class="membership profile">
		<?php if($member->images->first){?><img src="<?=$member->images->first->url?>" alt="<?=$member->title?> logo" />
		<?php } ?>
		<div class="description">
			<p><strong><?=$member->title?></strong><br />
				<?=$member->body?>
				<a href="<?=$member->link?>" title="<?=$member->title?> Website" target="_blank"><svg><use xlink:href="#ipdaIcon"></use></svg> Visit Website</a>
			</p>
		</div><!--desription-->
	</div><!--profile-->
<?php }
}?>

<div class="col-wrapper">
	<div class="col">
		<?=$page->column_1?>
	</div><!--col-->
	<div class="col">
		<?=$page->column_2?>
	</div><!--col-->
	<div class="col">
		<?=$page->column_3?>
	</div><!--col-->
</div><!--col-wrapper-->

<?php include 'inc/foot.php';?>