<?php
// HOMEPAGE TEMPLATE
include 'inc/head.php'; ?>

<?=$page->body?>

<div class="col">
	<svg><use xlink:href="#gears"></use></svg>
	<?=$page->column_1?>
</div><!--col-->
<div class="col">
	<svg><use xlink:href="#puzzle"></use></svg>
	<?=$page->column_2?>
	<a class="green-btn" href="<?=$pages->get('/member-resources/')->url?>" title="View Member Resource Gallery">View Members-Only Content</a>
</div><!--col-->
<div class="col">
	<svg><use xlink:href="#buildings"></use></svg>
	<?=$page->column_3?>
</div><!--col-->


<?php include 'inc/foot.php'; ?>